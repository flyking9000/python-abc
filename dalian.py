#!/usr/bin/python3
# -*- coding: utf-8 -*-

from yf import Gps
from yf import Mysql
from yf import Selenium


# 大连数据库查询
class QueryDalian:

    query = Mysql.QueryDb()

    def query_school(self):
        sql = "select * from `midSchool` limit 2"
        return self.query.query_mysql(sql)

    def query_lnglat(self):
        sql = "SELECT id,lng,lat FROM `primarySchool` "
        return self.query.query_mysql(sql)

    def update_lnglat(self, id, lng, lat):
        sql = 'update primarySchool set `lng-g`="'+lng+'" ,`lat-g`="'+lat+'" where id='+id
        print(sql)
        self.query.update_mysql(sql)

    def update_gps(self):
        data = self.query_lnglat()
        for row in data:
            d = Gps.Gps().wgs84_to_gcj02(float(row[1]), float(row[2]))
            self.update_lnglat(str(row[0]), str(d[0]), str(d[1]))

    def main(self):
        print("main方法启动   -------------------------------------------------")
        #c = self.query_school()
        #print(c)
        Selenium.SeleniumUtil().tianjin_login()
        #self.update_gps()


# 启动
QueryDalian().main()

