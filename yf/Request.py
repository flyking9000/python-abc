#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import json
import hashlib
import pandas as pd


class TianJin:

    '用于登陆的token，登陆的时候获取 '
    username = "admin"
    password = "admin"
    token = ""

    '登陆接口'
    def login(self):
        password = hashlib.md5(self.password.encode('utf-8')).hexdigest()
        c = requests.get('http://192.168.6.232:9191/tj/login', params={'userName': self.username, 'password': password})
        t = str(c.content, 'utf8')
        print(t)
        data = json.loads(t)
        self.token = data["data"]["token"]

    '地址匹配'
    def check_price(self, p):
        #c = requests.post('http://192.168.6.232:9191/tj/matchAddress', data={'token': self.token, 'road': '解放南路', 'roadNum': '283号', 'buildType': '商铺'})
        c = requests.post('http://192.168.6.232:9191/tj/matchAddress', data={'token': self.token, 'project': p["小区名称"], 'buildType': p["用途（标准）"]})
        c = str(c.content, 'utf8')
        data = json.loads(c)
        #print(data)
        paramm = {'token': self.token, 'buildType': p["用途（标准）"], 'houseStruct': p["建筑结构"], 'TotalFloor':p["总层数"],
                  'TotalArea': p["建筑面积"], 'area': p["建筑面积"], 'floor': p["所在层数"], 'reportPrice': '1', 'buildYear': p["建筑年代"],
                  'basePriceUUID': data["data"]["basePriceUUID"], 'matchlogId': data["data"]["matchlogId"], 'houseType': '非居住'}
        #print(paramm)
        c = requests.get('http://192.168.6.232:9191/tj/checkPrice', params=paramm)
        c = str(c.content, 'utf8')
        #print(c)
        data = json.loads(c)
        #print(data)
        return data

    '登陆后从excel取出数据进行地址匹配'
    def excel_check_price(self):
        self.login()
        p = pd.read_excel('../resouce/测试.xlsx', 0)
        for index, row in p.iterrows():
            a1 = row["小区名称"]
            try:
                data = self.check_price(row)
                print(data)
                a2 = data["data"]["evaluateTotalPrice"]
            except Exception:
                a2 = 0
            #print(index, '\t', a1, '\t', a2)
            row["核价总价"] = a2
            p.loc[index] = row
        p.to_excel('../resouce/测试13.xlsx', index=False, header=True)

    '主入口'
    def main(self):
        self.excel_check_price()


TianJin().main()


