#!/usr/bin/python3
# -*- coding: utf-8 -*-

import MySQLdb


class QueryDb:

    def get_connect(self):
        return MySQLdb.connect("192.168.6.232", "root", "root", "dalian", 3306, charset='utf8')

    def query_mysql(self, sql):
        # 打开数据库连接
        db = self.get_connect()
        # 使用cursor()方法获取操作游标
        cursor = db.cursor()
        # 使用execute方法执行SQL语句
        cursor.execute(sql)
        # 使用 fetchone() 方法获取一条数据
        # data = cursor.fetchone()
        data = cursor.fetchall()
        # 关闭数据库连接
        db.close()
        return data

    def update_mysql(self, sql):
        # 打开数据库连接
        db = self.get_connect()
        cursor = db.cursor()
        cursor.execute(sql)
        db.commit()
        '''
        try:
            cursor.execute(sql)
            db.commit()
        except:
            db.rollback()
        '''
        db.close()
