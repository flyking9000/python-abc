#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
@desc 工具类，签名
@auth wangfei
@time 2019-07-28
"""

import hashlib



"""
计算文件的md5值
"""


def md5_suan(filepath):
    f = open(filepath, "rb")
    m = hashlib.md5()
    while True:
        b = f.read(1024)
        if b:
            m.update(b)
        else:
            break
    f.close()
    c = m.hexdigest()
    return c


"""
计算文件的sha256值
"""


def sha256_suan(filepath):
    f = open(filepath, "rb")
    m = hashlib.sha256()
    m.update(f.read())
    f.close()
    c = m.hexdigest()
    return c



